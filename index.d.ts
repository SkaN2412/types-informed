
// Type definitions for informed 1.4.0
// Project: https://github.com/joepuzzo/informed#readme
// Definitions by: Daniel Pfisterer <https://github.com/pure180>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.6

import * as React from 'react';
import { EventEmitter } from 'events';

declare module 'informed' {
  
  /*
   * HELPER                                                               *
   * ###################################################################### *
   */

  export type FormValue<T =  string | number | boolean> = T
  export type FormError = string | undefined;
  export interface Nested<T> {
      [key: string]: T | Nested<T> | T[] | Nested<T>[];
  }
  export type FormValues<T = {}> = T | Nested<T>;
  export type Touched = Nested<boolean>;
  export interface FormErrors{
      [key: string]: FormError;
  }
  export type NestedErrors = Nested<FormErrors>;
  export type FormRenderReturn<C> = React.Context<C> | false | null | never[];
  export type RenderReturn = JSX.Element | React.ReactNode | false | null | never[];

  export type FieldHOC<T, P> =  
  (Component: React.RefForwardingComponent<T, P>) => 
    React.PureComponent<P>;

  export type FormHocComponent<T, P> = React.ReactNode | React.RefForwardingComponent<T, P>;

  export type FormClassHOC<Class, P = {}> =  
    (Component: Class) => 
      React.PureComponent<P>;
  

  /*
   * FORM                                                                   *
   * ###################################################################### *
   */

  export interface FormProps<T = {}> {
    className?: string;
    onChange?: (formState: FormState<T>) => void;
    onSubmit?: (values: FormValues<T>) => void;
    onValueChange?: (value: FormValue) => void;
    preSubmit?: (values: FormValues<T>) => void;
    getApi?: (api?: FormApi<T>) => void;
    dontPreventDefault?: boolean;
    onSubmitFailure?: Function;
    initialValues?: FormValues<T>;
  }

  export interface FormSharedProps {
    component?: React.ReactType<FormClassContext>;
    deregister?: (field?: string) => void;
    register?: (field?: string, fieldController?: any) => void;  // FIXME: Replace any
    render?: FormRenderReturn<FormClassContext>;
  }

  export interface FormValueState<T = {}> {
    pristine: boolean;
    touched: Touched | {};
    values: FormValues<T> | {};
  }

  export interface FormState<T = {}> extends FormValueState<T> {
    dirty?: boolean;
    errors: FormErrors | {};
    invalid: boolean;
  }

  /**
   * FormApi
   * =====
   * Informed gives you access to a formApi!
   * This api allows you to grab and manipulate values using getters and setters. Below is a table that describes each function available within the formApi.
   */
  export interface FormApi<T = {}> {

    /**
     * getError()
     * ======
     * Function that when given a field name will return its error.
     * @param {string} field Id of field
     * @returns {Object} FormValue
     */
    getError: (field: string) => FormValue;

    /**
     * getState()
     * ======
     * Function that returns the formState. Note this will only return the state as it existed when the function was called.
     * @returns {Object} FormState
     */
    getState: () => FormState<T>;

    /**
     * getTouched()
     * ======
     * Function that when given a field name will return whether or not its touched.
     * @param {string} field Id of field
     * @returns {Object} FormValue
     */
    getTouched: (field: string) => FormValue;

    /**
     * getValue()
     * ======
     * Function that when given a field name will return its value.
     * @param {string} field Id of field
     * @returns {Object} FormValue
     */
    getValue: (field: string) => FormValue;

    /**
     * reset()
     * ======
     * Function that will reset the form to its initial state.
     * @returns {Object} void
     */
    reset: () => void;

    /**
     * setError()
     * ======
     * Function that takes two parameters, the first is the field name, and the second is the error message you want to set it to.
     * @param {string} field Id of field
     * @param {string} value Error message
     * @returns void
     */
    setError: (field: string, value: string) => void; 

    /**
     * setState()
     * ======
     * Function that sets the whole form state. Note, you can NOT set derived values such as pristine. See State section of the docs for additional details.
     * @param {FormValueState} state FormValueState
     * @returns void
     */
    setState: (state: FormValueState<T>) => void;

    /**
     * setTouched()
     * ======
     * Function that takes two parameters, the first is the field name, and the second is true or false.
     * @param {string} field 
     * @param {boolean} value
     * @returns void
     */
    setTouched: (field: string, value: boolean) => void;

    /**
     * setValue()
     * ======
     * Function that takes two parameters, the first is the field name, and the second is the value you want to set it to.
     * @param {string} field 
     * @param {FormValue} value
     * @returns void
     */
    setValue: (field: string, value: FormValue) => void; 

    /**
     * setValues()
     * ======
     * Function that will set the form values.
     * @param {FormValue} values
     * @returns void
     */
    setValues: (values: FormValues<T>) => void;

    /**
     * submitForm()
     * ======
     * This function will submit the form and trigger some lifecycle events. 
     * 1. It will set all the fields to touched. 
     * 2. It will call all validators. 
     * 3. It will call onSubmit if the form is valid. This function can be called manually however it is also called if you have a `<button type='submit'>` within the `<Form>`.
     * @param {FormValue} values
     * @returns void
     */
    submitForm: (values: FormValues<T>) => void;
  }

  
  export interface FormClassApi<T = {}> extends FormApi<T> {
    getFullField: (field: string) => FormValue;
    notify: (field: FormValues<T>) => void;
  }

  export interface FormApiContext<T = {}> {
    controller: FormController;
    formApi: FormClassApi<T>;
    formState: FormState<T>;
  }

  export interface FormClassContext extends FormApiContext {
    component?: React.ReactType<{ fieldApi: FieldApi }>;
  }
  
  /**
   * Form
   * =====
   */
  export class Form extends React.Component<
    FormProps & { children?: ((props: FormClassContext) => RenderReturn) | RenderReturn }
  > {
    content: RenderReturn;
    controller: FormControllerProps;
    formContext: FormClassContext;
  }

  /*
   * FIELDS                                                                 *
   * ###################################################################### *
  */

  export interface FieldApi<T = {}> {
    getValue?: () => FormApi<T>['getValue'];
    setValue?: () => FormApi<T>['setValue'];
    getTouched?: () => FormApi<T>['getTouched'];
    setTouched?: () => FormApi<T>['setTouched'];
    getError?: () => FormApi<T>['getError'];
    setError?: () => FormApi<T>['setError'];
  }

  export interface FieldState {
    value: FormValue;
    touched: boolean;
    error: string;
  }

  export interface FieldContext {
    fieldApi: FieldApi;
    fieldState: FieldState
  }

  export interface FieldDefaultShared {
    initialValue?: string | number;
    mask?: (value: string | number) => any;
    notify?: string[] | number[];
    validate?: (value: FormValue, values?: any) => FormError | null;
    validateOnBlur?: boolean;
    validateOnChange?: boolean;
    validateOnMount?: boolean;
  }

  export interface FieldDefaultProps {
    field: string;
  }

  export interface BasicFieldContext {
    fieldApi?: FieldApi;
    fieldState?: FieldState;
  }

  export type FieldProps = FieldDefaultProps & FieldDefaultShared;
  export type BasicFieldProps = BasicFieldContext & FieldDefaultShared;

  export type BasicField = React.StatelessComponent<BasicFieldProps & React.InputHTMLAttributes<HTMLInputElement>>;

  /**
   * Checkbox
   * =====
   * Below are all the input props that informed's inputs accept.
   * @param {string} field Every input must have a field. This is how the form manages the state of this input. See the field syntax section below for additional details on what you can pass in for field.
   * @param {string} initialValue An initial value for that field.
   * @param {function} validate Function that gets called when form performs validation. Function accepts the value as a parameter and must return either an error string or null. By default it only gets called onSubmit. See Validation section for additional details.
   * @param {function} validateOnBlur Tells field to perform validation onBlur. By default it only validates onSubmit.
   * @param {function} validateOnChange Tells field to perform validation onChange. By default it only validates onSubmit.
   * @param {function} validateOnMount Tells field to perform validation onMount.
   * @param {function} mask Function that will mask values when entered. Example `value => value + '!!!'` or `value => value.trim()`
   * @param {InputHTMLAttributes} InputHTMLAttributes All inputs can accept any props that a native html input, select, textarea, etc. can accept. For example, if you want to disable a text input, you would simply pass disabled
   * @returns HTMLInputElement
   */
  export const Checkbox: FieldHOC<FieldContext, FieldProps & React.InputHTMLAttributes<HTMLInputElement>>;

  /**
   * BasicCheckbox
   * =====
   * TODO: Add documentations
   */
  export const BasicCheckbox: BasicField;

  /**
   * Radio
   * =====
   * Below are all the input props that informed's inputs accept.
   * @param {InputHTMLAttributes} InputHTMLAttributes All inputs can accept any props that a native html input, select, textarea, etc. can accept. For example, if you want to disable a text input, you would simply pass disabled
   * @returns HTMLInputElement
   */
  export const Radio: FieldHOC<FieldContext, React.InputHTMLAttributes<HTMLInputElement>>;

  export const BasicRadio: BasicField;

  /**
   * Text
   * =====
   * Below are all the input props that informed's inputs accept.
   * @param {string} field Every input must have a field. This is how the form manages the state of this input. See the field syntax section below for additional details on what you can pass in for field.
   * @param {string} initialValue An initial value for that field.
   * @param {function} validate Function that gets called when form performs validation. Function accepts the value as a parameter and must return either an error string or null. By default it only gets called onSubmit. See Validation section for additional details.
   * @param {function} validateOnBlur Tells field to perform validation onBlur. By default it only validates onSubmit.
   * @param {function} validateOnChange Tells field to perform validation onChange. By default it only validates onSubmit.
   * @param {function} validateOnMount Tells field to perform validation onMount.
   * @param {function} mask Function that will mask values when entered. Example `value => value + '!!!'` or `value => value.trim()`
   * @param {InputHTMLAttributes} InputHTMLAttributes All inputs can accept any props that a native html input, select, textarea, etc. can accept. For example, if you want to disable a text input, you would simply pass disabled
   * @returns HTMLInputElement
   */
  export const Text: FieldHOC<FieldContext, FieldProps & React.InputHTMLAttributes<HTMLInputElement>>;

  /**
   * BasicText
   * =====
   * TODO: Add documentations
   */
  export const BasicText: BasicField;

  /**
   * TextArea
   * =====
   * Below are all the input props that informed's inputs accept.
   * @param {string} field Every input must have a field. This is how the form manages the state of this input. See the field syntax section below for additional details on what you can pass in for field.
   * @param {string} initialValue An initial value for that field.
   * @param {function} validate Function that gets called when form performs validation. Function accepts the value as a parameter and must return either an error string or null. By default it only gets called onSubmit. See Validation section for additional details.
   * @param {function} validateOnBlur Tells field to perform validation onBlur. By default it only validates onSubmit.
   * @param {function} validateOnChange Tells field to perform validation onChange. By default it only validates onSubmit.
   * @param {function} validateOnMount Tells field to perform validation onMount.
   * @param {function} mask Function that will mask values when entered. Example `value => value + '!!!'` or `value => value.trim()`
   * @param {InputHTMLAttributes} InputHTMLAttributes All inputs can accept any props that a native html input, select, textarea, etc. can accept. For example, if you want to disable a text input, you would simply pass disabled
   * @returns HTMLInputElement
   */
  export const TextArea: FieldHOC<FieldContext, FieldProps & React.InputHTMLAttributes<HTMLInputElement>>;

  /**
   * BasicTextArea
   * =====
   * TODO: Add documentations
   */
  export const BasicTextArea: BasicField;

  export interface RadioGroupProps extends RadioGroupApi {
    field: string;
    children?: ((props: FieldContext) => RenderReturn) | RenderReturn;
  }

  export interface RadioGroupApi extends FieldApi {
    onChange?: any;
    onBlur?: any;
  }

  export interface RadioGroupContext {
    radioGroupApi: RadioGroupApi;
    radioGroupState: FieldState;
  }

  class RadioGroupClass extends React.Component<RadioGroupProps & FieldState> {
    groupContext: RadioGroupContext;
  }

  /**
   * RadioGroup
   * =====
   * Below are all the input props that informed's inputs accept.
   * @param {string} field Every input must have a field. This is how the form manages the state of this input. See the field syntax section below for additional details on what you can pass in for field.
   * @param {string} initialValue An initial value for that field.
   * @param {function} validate Function that gets called when form performs validation. Function accepts the value as a parameter and must return either an error string or null. By default it only gets called onSubmit. See Validation section for additional details.
   * @param {function} validateOnBlur Tells field to perform validation onBlur. By default it only validates onSubmit.
   * @param {function} validateOnChange Tells field to perform validation onChange. By default it only validates onSubmit.
   * @param {function} validateOnMount Tells field to perform validation onMount.
   * @param {function} mask Function that will mask values when entered. Example `value => value + '!!!'` or `value => value.trim()`
   * @param {InputHTMLAttributes} InputHTMLAttributes All inputs can accept any props that a native html input, select, textarea, etc. can accept. For example, if you want to disable a text input, you would simply pass disabled
   * @returns HTMLInputElement
   */
  export const RadioGroup: FormClassHOC<RadioGroupClass, RadioGroupProps & FieldProps>;

  export interface OptionProps extends React.OptionHTMLAttributes<HTMLOptionElement> {
    children: RenderReturn;
    ref?: React.Ref<any>; // FIXME: Replace any
    key?: React.Key;
  }
  export const Option: React.StatelessComponent<OptionProps>;

  export interface SelectClassProps extends React.SelectHTMLAttributes<HTMLSelectElement> {
    children?: ((props: FieldContext) => RenderReturn) | RenderReturn;
    field: string;
    forwardedRef?: any; // FIXME: Replace any; 
    multiple?: boolean;
  }

  export type SelectProps = FieldProps & FieldContext & SelectClassProps

  export class BasicSelect extends React.Component<SelectProps> {
    handleChange(e: React.SyntheticEvent<HTMLSelectElement>): void;
  }

  export const Select: FormClassHOC<BasicSelect, FieldProps & SelectClassProps>

  export interface FieldClassProps extends FormSharedProps, FieldDefaultShared {
    children?: ((props: FormClassContext) => RenderReturn) | RenderReturn;
    debug?: boolean;
    field: string;
  }

  export interface FieldClassReturn extends FieldContext {
    forwardedRef?: any; // FIXME: Replace any; 
  }

  class FieldClass extends React.PureComponent<FieldClassReturn & FieldClassProps> {
    me: React.RefObject<any>; // FIXME: Replace any.
  }

  export const Field: FormClassHOC<FieldClass, FieldClassProps>


  export interface ScopedContext extends FormApiContext {}

  export interface ScopeProps {
    children?: ((props: FormClassContext) => RenderReturn) | RenderReturn;
    scope: string | number;
  }

  class ScopeClass extends React.Component<ScopeProps & ScopedContext> {
    formContext: ScopedContext
  }

  export const Scope: FormClassHOC<ScopeClass, ScopeProps>;

  /*
   * ADDITIONAL                                                             *
   * ###################################################################### *
   */

  /*
   * Declarations for FormController
   * ./src/Controller/FormController.js
   */

  export interface FormControllerHooks<T = {}> {
    getApi?: (api: FormApi<T>) => FormApi<T>;
    onSubmit?: any; // FIXME: Replace any
    onSubmitFailure?: any; // FIXME: Replace any
    preSubmit?: (formValues: FormValue) => void; // FIXME: Replace any
    // validate: any; // FIXME: Replace any
  }

  export interface FormControllerConfig {
    dontPreventDefault?: boolean;
    initialValues?: FormValues;
  }

  export interface FormControllerProps<T = {}> extends FormSharedProps {
    api: FormApi<T>;
    config: FormControllerConfig;
    errors: ObjectMapProps<FormErrors>;
    dirty: () => boolean;
    fields: Map<any, any>; // FIXME: Replace any
    getError: FormApi<T>['getError'];
    getFormState: () => FormValues;
    getFullField: FormClassApi<T>['getFullField'];
    getTouched: FormApi<T>['getTouched'];
    getValue: FormApi<T>['getValue'];
    hooks: FormControllerHooks<T>;
    invalid:() => boolean;
    notify: FormClassApi<T>['notify'];
    pristine: () => boolean;
    remove: (field: string) => void;
    reset: FormApi<T>['reset'];
    setError: FormApi<T>['setError']; 
    setFormState: (state: FormState<T>) => void;
    setTouched: FormApi<T>['setTouched'];
    setValue: FormApi<T>['setValue'];
    setValues: FormApi<T>['setValues'];
    state: () => FormState<T>;
    submitForm: FormApi<T>['submitForm'];
    touched: ObjectMapProps<Touched>;
    valid:() => boolean;
    values: ObjectMapProps<FormValues>;
  }

  export class FormController<T = {}> extends EventEmitter implements FormControllerProps {
    api: FormApi<T>;
    config: FormControllerConfig;
    deregister: (field: string) => void;
    dirty: () => boolean;
    errors: ObjectMapProps<FormErrors>;
    fields: Map<any, any>  // FIXME: Replace any
    getError: FormApi<T>['getError'];
    getFormState: () => FormValues<T>;
    getFullField: FormClassApi<T>['getFullField'];
    getTouched: FormApi<T>['getTouched'];
    getValue: FormApi<T>['getValue'];
    hooks: FormControllerHooks<T>;
    invalid:() => boolean;
    notify: FormClassApi<T>['notify'];
    pristine: () => boolean;
    register: (field: string, fieldController: any) => void;  // FIXME: Replace any
    remove: (field: string) => void;
    reset: FormApi<T>['reset'];
    setError: FormApi<T>['setError']; 
    setFormState: (state: FormState<T>) => void;
    setTouched: FormApi<T>['setTouched'];
    setValues: FormApi<T>['setValues'];
    setValue: FormApi<T>['setValue'];
    state: () => FormState<T>;
    submitForm: FormApi<T>['submitForm'];
    touched: ObjectMapProps<Touched>;
    valid:() => boolean;
    values: ObjectMapProps<FormValues>;
  }

  /*
   * Declarations for ObjectMap
   * ./src/Controller/index.js
   */

  export function buildMap(cursor: FormValues | FormValues[]): FormValues | FormValues[];

  export interface ObjectMapProps<V> {
    delete(path: any): void; // FIXME: Replace any
    empty(): boolean;
    get(path: any): any; // FIXME: Replace any
    map: V;
    object: V;
    path: Map<any, any>; // FIXME: Replace any
    set(path: any, value: any ): void; // FIXME: Replace any
    rebuild(object: V): void;
  }

  export class ObjectMap<V> implements ObjectMapProps<V> {
    constructor(object: V);
    delete(path: any): void; // FIXME: Replace any
    empty(): boolean;
    get(path: any): any; // FIXME: Replace any
    map: V;
    object: V;
    path: Map<any, any>; // FIXME: Replace any
    set(path: any, value: any ): void; // FIXME: Replace any
    rebuild(object: V): void;
  }
  
  /*
    * Declarations for asField
    * ./src/HOC/asField.js
    */

  export interface AsFieldReturn<P> {
    (props: P): React.PureComponent<P>;
    displayName: 'string'
  }
  export function asField<T, P>(Component: React.RefForwardingComponent<T, P & FieldContext>): AsFieldReturn<P>;

  /*
    * Declarations for withFormApi
    * ./src/HOC/withFormApi.js
    */

  export function withFormApi<T, P = {}>(Component: FormHocComponent<T, P & {formApi: FormApi}>): React.ComponentType<React.ClassAttributes<T> & P>;

  /*
    * Declarations for withFormState
    * ./src/HOC/withFormState.js
    */

  export function withFormState<T, P = {}>(Component: FormHocComponent<T, P & {formState: FormState}>): React.ComponentType<React.ClassAttributes<T> & P>;

  /*
    * Declarations for field HOC's
    * ./src/HOC/withFieldStuff.js
    */

  export function withFieldApi<T, P>(field: string):
    (Component: FormHocComponent<T, P & {fieldApi: FieldApi}>) =>
      React.ComponentType<React.ClassAttributes<T> & P>;

  export function withFieldState<T, P>(field: string):
    (Component: FormHocComponent<T, P & {fieldState: FieldState}>) =>
      React.ComponentType<React.ClassAttributes<T> & P>;

  /*
  * Declarations for withRadioGroup
  * ./src/HOC/withRadioGroup.js
  */

  export function withRadioGroup<T, P = {}>(Component: FormHocComponent<T, P & RadioGroupContext>): React.ComponentType<React.ClassAttributes<T> & P>;
}