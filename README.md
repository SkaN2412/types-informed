TypeScript definitions for `informed`
==

This project contains the TypeScript type definitions for the [`informed`](https://github.com/joepuzzo/informed) project.

The version number of this project should track the version number of `informed` as closely as possible.

